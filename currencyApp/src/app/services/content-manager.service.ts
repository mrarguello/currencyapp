import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()

export class ContentManagerService {

	constructor(private http: HttpClient) {
	}
	
	keys:string[]=[];
	keysNoBase:string[]=[];
	currentValue:number;
	currentRateMessage:string = ""

	selectedCurrencyRates:ConversionRates = {base:"",date:"",'rates':{}};
	selectedCurrencyBase:string = "";
	selectedCurrencyPair:string = "";
	exchangeResult:number;
	
	loadedOk:boolean = false;
	spinner:boolean = true;
	error:boolean = false;	

	getCurrencies(){
		let self = this;
		this.http.get('https://api.fixer.io/latest').subscribe((data:ConversionRates) => {
			self.selectedCurrencyRates = data;			
			self.selectedCurrencyBase = data.base;
			self.keys.push(self.selectedCurrencyRates.base);
			self.keys = self.keys.concat(Object.keys(self.selectedCurrencyRates.rates));
			self.keysNoBase = self.keysNoBase.concat(Object.keys(self.selectedCurrencyRates.rates));
			self.selectedCurrencyPair= Object.keys(self.selectedCurrencyRates.rates)[0];
			self.loadedOk=true;
			self.spinner = false;
			self.error = false;
			self.currentRateMessage = 'Select the currency you want to convert.';
		}, err => {
			self.error = true;
			self.spinner = false;
      		self.currentRateMessage = 'There was a problem loading the app. Please refresh your browser.';
      	});	
	}

	updateBase(base){
		let self = this;
		this.spinner = true;
		this.http.get('https://api.fixer.io/latest?base='+base).subscribe((data:ConversionRates) => {
			self.spinner = false;
			self.selectedCurrencyRates = data;
			self.keysNoBase = Object.keys(self.selectedCurrencyRates.rates);
			self.selectedCurrencyPair = Object.keys(self.selectedCurrencyRates.rates)[0];
			self.selectedCurrencyBase = data.base;
			self.calculateCurrency(self.currentValue);
		}, err => {
			self.spinner = false;
			self.error = true;			
      		self.currentRateMessage = 'Could not update rates. Please try again.';
      		setTimeout(()=>{ self.error = false; self.currentRateMessage = "Select the currency you want to convert.";  }, 2000)	
      	});	
	}

	updatePair(pair){
		this.selectedCurrencyPair = pair;
		this.calculateCurrency(this.currentValue);
	}

	calculateCurrency(amount:number){
			this.currentValue = amount;
			this.exchangeResult = amount * this.selectedCurrencyRates.rates[this.selectedCurrencyPair]
			this.currentRateMessage = "Conversion Rate: " + "1 " + this.selectedCurrencyBase + " = " + this.selectedCurrencyRates.rates[this.selectedCurrencyPair] + " " + this.selectedCurrencyPair;
	}

	swapCurrencies(){
		let temp="";
		temp = this.selectedCurrencyBase;
		this.selectedCurrencyBase = this.selectedCurrencyPair;
		this.selectedCurrencyPair = temp;
		this.updateBase(this.selectedCurrencyBase);
		this.calculateCurrency(this.currentValue);
	}
}


interface ConversionRates{
	base:string,
	date:string,
	rates:{}
} 