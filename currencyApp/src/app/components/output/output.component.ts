import { Component, OnInit, Input } from '@angular/core';
import { ContentManagerService } from '../../services/content-manager.service';


@Component({
	selector: 'app-output',
	templateUrl: './output.component.html',
	styleUrls: ['./output.component.scss']
})

export class OutputComponent implements OnInit {
	contentManagerService : ContentManagerService;
	constructor(c:ContentManagerService) {
		this.contentManagerService = c;
	}

	newPair(pair){
		this.contentManagerService.updatePair(pair);
	}

	ngOnInit() {
		//hacer un pipe en outputComponent para filtrar el que figure como base.
		//hacer que se le pueda hacer un input al value
	}
}
