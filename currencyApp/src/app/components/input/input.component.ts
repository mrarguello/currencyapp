import { Component, OnInit } from '@angular/core';
import { ContentManagerService } from '../../services/content-manager.service';

@Component({
	selector: 'app-input',
	templateUrl: './input.component.html',
	styleUrls: ['./input.component.scss']
})

export class InputComponent implements OnInit {
	contentManagerService : ContentManagerService;	
	notNumber:boolean = false;	

	constructor(c:ContentManagerService) { 
		this.contentManagerService = c;
	}

	exchange(amount:any){
		if(amount !== ''){			
			this.notNumber = false;
			this.contentManagerService.calculateCurrency(amount);
		} else {
			this.contentManagerService.exchangeResult = 0;
			this.notNumber = true;
		}
	}

	newBase(base){
		this.contentManagerService.updateBase(base);
	}

	ngOnInit() {

	}

}
