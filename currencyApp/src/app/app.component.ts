import { Component, OnInit } from '@angular/core';
import { ContentManagerService } from './services/content-manager.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';

  contentManagerService : ContentManagerService;

  constructor(c:ContentManagerService){
  	this.contentManagerService = c;
  }

  ngOnInit(){
  	this.contentManagerService.getCurrencies();
  }
}
