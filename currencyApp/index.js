var static = require('node-static');
var file = new static.Server();

require('http').createServer(function(request, response) {
  request.addListener('end', function() {
    file.serve(request, response);
  }).resume();
}).listen(process.env.PORT || 3000);

// const express = require('express')
// const path = require('path')
// const PORT = process.env.PORT || 5000

// express()
//   .use(express.static(path.join(__dirname, 'public')))
//   .set('src', path.join(__dirname, 'src'))
//   .set('view engine', 'ejs')
//   .get('/', (req, res) => res.render('index'))
// .listen(PORT, () => console.log(`Listening on ${ PORT }`))